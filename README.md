# ContentEditor

## Bauen des Projekts
! Das Projekt bitte nur mit den NPM-Tasks `build-electron` (NICHT electron-build) und `electron` benutzen! `build-electron` zum Bauen der Electron-App und `electron` zum Ausführen der App.

## Development server

Mit `ng serve` wird ein dev Server gestartet auf `http://localhost:4200/`. Die App updated sich automatisch, wenn gespeichert wird

## Electron build

Mit `npm run electron-build` erfolgt das Bauen der Angular App als Electron app und diese wird ausgeführt 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.4.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

