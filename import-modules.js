if(require) {
  const traversdir = require('traversdir');
  let fs = require("fs");
  let path = require("path");

  window.listFolderStructure = traversdir;

  window.saveFile = function(path, content) {
    ensureDirectoryExistence(path);
    fs.writeFileSync(path, content , {"encoding" : "utf-8"});
  };

  window.loadFile = function(path) {
    let content = fs.readFileSync(path);
    return content.toString();
  };

  function ensureDirectoryExistence(filePath) {
    let dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
      return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
  }

}
