import {EventEmitter, Injectable, Output} from "@angular/core";

@Injectable()
export class RefreshService {

  @Output() change: EventEmitter<object> = new EventEmitter();

  public triggerRefresh(path, content) {
    const obj = {"path" : path, "content" : content};
    this.change.emit(obj);
  }

}
