import {Injectable} from "@angular/core";

@Injectable()
export class PathService {

  public projectPath;
  public doctypePath = "\\assets\\html\\doctype.json";
  public contentPath = "\\assets\\html\\content";

  public getProjectPath() {
    return this.projectPath;
  }

  public setProjectPath(projectPath: string) {
    this.projectPath = projectPath;
  }

}
