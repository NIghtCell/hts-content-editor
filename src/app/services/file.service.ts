import { Injectable } from '@angular/core';

@Injectable()
export class FileService {

  private doctypeContent: any;

  public getDoctypeContent(): any {
    return this.doctypeContent;
  }

  public setDoctypeContent(value) {
    this.doctypeContent = value;
  }

}
