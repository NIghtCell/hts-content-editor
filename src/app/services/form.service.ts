import { Injectable } from "@angular/core";

@Injectable()
export class FormService {
  constructor() { }
  public _activeFormData = null;

  public docTypeData = null;
  private _docTypes = null;
  public activeDocTypeData = null;
  public onChange: Function;

  set activeFormData(value: any) {
    value = JSON.parse(value);
    this._activeFormData = value;
    this.setActiveDocTypeData();
  }

  set docTypes(value: any) {
    value = JSON.parse(value);
    this._docTypes = value;
  }

  public change(data) {
    this.docTypeData = data;
    this.onChange();
  }

  private setActiveDocTypeData() {
    for (const doctype in this._docTypes) {
      if (this._docTypes.hasOwnProperty(doctype)) {
        const docTypeName = this.getDocTypeName();
        const currentDocTypeName = this._docTypes[doctype]["name"];
        if (currentDocTypeName === docTypeName) {
          this.activeDocTypeData = this._docTypes[doctype];
        }
      }
    }
  }

  private getDocTypeName() {
    const templateName = this._activeFormData["template"];
    return this.getTypeName(templateName);
  }

  private getTypeName(templateName: any) {
    return templateName.split("/").pop();
  }
}
