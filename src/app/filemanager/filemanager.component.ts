import { Component, OnInit } from "@angular/core";
import {Message} from "primeng/api";
import {FileService} from "../services/file.service";
import {FormService} from '../services/form.service';

@Component({
  selector: "app-filemanager",
  templateUrl: "./filemanager.component.html",
  styleUrls: ["./filemanager.component.css"]
})
export class FilemanagerComponent implements OnInit {

  constructor(public fileService: FileService, public formService: FormService) {}

  msgs: Message[];

  uploadedFiles: any[] = [];

  onFileUpload(event) {
    this.storeFile(event);
    this.saveFileContent();

    this.msgs = [];
    this.msgs.push({severity: "info", summary: "File Uploaded", detail: ""});
  }

  private saveFileContent() {
    this.readFile().then((data) => {
      this.fileService.setDoctypeContent(data);
    });
  }

  private async readFile() {
    const reader = new FileReader();
    reader.readAsText(this.uploadedFiles[0]);
    const doctypeContent = await this.loadDoctypeContent(reader);
    this.formService.docTypes = doctypeContent;
    return doctypeContent;
  }

  private loadDoctypeContent(reader: FileReader) {
    return new Promise(resolve => {
    reader.onload = (evt) => {
      evt.preventDefault();
      resolve(reader.result);
    };
  });
  }

  private storeFile(event): void {
    for (const file of event.files) {
      this.uploadedFiles[0] = file;
    }
  }

  ngOnInit() {
  }

}
