import {Component, OnInit} from "@angular/core";
import {MenuItem, TreeNode} from "primeng/api";
import {FormService} from "../services/form.service";
import {environment} from "../../environments/environment";
import {PathService} from "../services/path.service";
import {RefreshService} from "../services/refresh.service";

declare const loadFile: Function;
declare const listFolderStructure: Function;
declare const filestyle: Function;

const folders = "elements" || "modules" || "pages";

@Component({
  selector: "app-mainmenu",
  templateUrl: "./mainmenu.component.html"
})

export class MainmenuComponent implements OnInit {

  contentItems: MenuItem[];
  treeItems: TreeNode[] = [];
  currentTreeDirectory: any;
  doctypes: MenuItem[];
  noDoctypeError = "Keine Doctype Datei im Projekt gefunden, " +
    "bitte wählen Sie ein anderes Projekt aus";
  selectedNode: TreeNode;

  constructor(protected formService: FormService, public pathService: PathService,
              protected refreshService: RefreshService) {
  }

  ngOnInit() {
    this.refreshService.change.subscribe((obj) => {
      const path = obj.path;
      const content = obj.content;
      this.currentTreeDirectory = this.treeItems[0];
      this.generateTreeView(true);
    });
  }

  public onNodeSelect(event) {
    const fileData = event.node.data;
    this.formService.activeFormData = fileData;
    this.formService.change(this.formService.activeDocTypeData);
    /* TODO: Pfad des Nodes an den Service schicken, damit die Form-Component diesen Pfad als ->
    Standardwert in den Save-Path schreiben kann und somit direkt an der gleichen Stelle speichern kann.*/
    console.log(this.formService._activeFormData);
    console.log(this.formService.activeDocTypeData);

  }

  public onSelectProject(event) {
    let projectPath;
    projectPath = event.target.files[0].path;
    this.pathService.setProjectPath(projectPath);
    this.generateMenu();
  }

  private createDocument(event) {
    this.formService._activeFormData = null;
    this.formService.change(event.item.automationId);
  }

  private convertDoctypesToMenuItems(docTypes: any, callback: Function): MenuItem[] {
    const items = [];
    for (const doctype of docTypes) {
      const item = {
        label: doctype.label,
        automationId: doctype,
        command: callback
      };
      items.push(item);
    }
    return items;
  }

  private getDocTypes(path: string = null) {
    if (environment.isElectron) {
      return loadFile(path);
    } else {
      return JSON.stringify([
        {
          "name": "teaser",
          "label": "Teaser",
          "type": "element",
          "fields": [
            {
              "id": "title",
              "label": "Titel",
              "type": "string"
            },
            {
              "id": "teaserText",
              "label": "Teaser-Text",
              "type": "richtext"
            }
          ],
          "variants": [
            "small"
          ]
        },
        {
          "name": "gallery",
          "label": "Galerie",
          "type": "module",
          "fields": [
            {
              "id": "items",
              "label": "Dateien",
              "type": "link"
            }
          ],
          "variants": []
        },
        {
          "name": "index",
          "label": "Index",
          "type": "page",
          "fields": [
            {
              "id": "title",
              "label": "Titel",
              "type": "string"
            },
            {
              "id": "items",
              "label": "Dateien",
              "type": "link"
            }
          ],
          "variants": []
        },
        {
          "name": "MitBoolean",
          "label": "Mit Boolean",
          "type": "element",
          "fields": [
            {
              "id": "someText",
              "label": "Some Text",
              "type": "string"
            },
            {
              "id": "boolsche",
              "label": "Boolsche",
              "type": "boolean"
            }
          ],
          "variants": []
        }
      ]);
    }
  }

  private generateNewButtonMenu() {
    let docs;
    try {
      docs = this.getDocTypes(this.pathService.projectPath +
        this.pathService.doctypePath);
      this.formService.docTypes = docs;
    } catch (e) {
      throw new Error((this.noDoctypeError));
    }
    docs = this.convertDoctypesIfElectron(docs);
    this.doctypes = this.convertDoctypesToMenuItems(docs, this.createDocument.bind(this));
    this.contentItems = [
      {
        label: "Neu",
        icon: "fa-plus",
        items: this.doctypes
      }
    ];
  }

  private convertDoctypesIfElectron(docs) {
    if (environment.isElectron) {
      docs = this.convertDoctypesToJSON(docs);
    }
    return docs;
  }

  private generateTreeView(expanded: boolean) {
    const currentDirectory = this.currentTreeDirectory;
    const projectPath = this.pathService.projectPath;
    const contentPath = this.pathService.contentPath;
    const structure = this.getFolderStructure(projectPath + contentPath);
    const directories = structure._dirs;
    this.createParentFolder(expanded);
    const treeDir = this.treeItems[0];
    this.generateDirectoriesAsTree(structure, directories, treeDir, true, currentDirectory);
    this.treeItems = JSON.parse(JSON.stringify(this.treeItems));
  }

  private createParentFolder(expanded: boolean) {
    this.treeItems = [
      {
        "label": "Bearbeiten",
        "data": "Content Type Folder",
        "expandedIcon": "fa-folder-open",
        "collapsedIcon": "fa-folder",
        "selectable": false,
        "expanded": expanded,
        "children": []
      }
    ];
  }

  private generateDirectoriesAsTree(structure: any, directories: any, treeDir: any,
                                    isRootFolder: boolean, currentDirectory: any) {
    let newStructure;
    let newDirectories;
    let newDir;
    for (const dir of directories) {
      if (dir === "elements" || dir === "modules" || dir === "pages" || !isRootFolder) {
        newStructure = structure;
        newDirectories = directories;
        newDir = treeDir;
        const directoryPath = structure[dir]["_path"];
        const files = this.createFilesOfDirectory(structure[dir]["_files"], directoryPath);
        const item = this.createTreeFolder(dir, files);
        treeDir.children.push(item);
        if (currentDirectory !== undefined) {
          this.copyExpandedStateOfTreeFolder(currentDirectory, treeDir);
        }
        if (newStructure[dir]._dirs.length > 0) {
          this.selectChildDirectoryAndRecallFunction(newStructure, dir, newDirectories,
            newDir, treeDir, currentDirectory);
        }
      }
    }
  }

  private copyExpandedStateOfTreeFolder(currentDirectory: any, treeDir: any) {
    const expanded = this.getExpandedStateOfTreeFolder(currentDirectory, treeDir);
    this.setExpandedStateOfTreeFolder(treeDir, expanded);
  }

  private setExpandedStateOfTreeFolder(treeDir: any, expanded: boolean) {
    const lastChild = treeDir.children[treeDir.children.length - 1];
    lastChild["expanded"] = expanded;
  }

  private getExpandedStateOfTreeFolder(currentDirectory: any, treeDir) {
    let expanded;
    if (currentDirectory !== undefined) {
      const index = treeDir.children.length - 1;
      expanded = this.checkIfFolderIsExpanded(currentDirectory, index);
    } else {
      expanded = false;
    }
    return expanded;
  }

  private selectChildDirectoryAndRecallFunction(newStructure, dir, newDirectories, newDir, treeDir: any, currentDirectory) {
    newStructure = newStructure[dir];
    newDirectories = newStructure._dirs;
    newDir = treeDir.children[treeDir.children.length - 1];
    if (currentDirectory !== undefined) {
      currentDirectory = currentDirectory.children[treeDir.children.length - 1];
    }
    const isRoot = false;
    this.generateDirectoriesAsTree(newStructure, newDirectories, newDir, isRoot, currentDirectory);
  }

  private createTreeFolder(dir, files: TreeNode[]) {
    const item = {
      "label": dir,
      "data": dir,
      "expandedIcon": "fa-folder-open",
      "collapsedIcon": "fa-folder",
      "selectable": false,
      "children": files,
    };
    return item;
  }

  private loadProjectFile(path): any {
    const contentPath = this.pathService.projectPath + this.pathService.contentPath;
    return loadFile(contentPath + path);
  }

  private createFilesOfDirectory(dir: string[], directoryPath: string): TreeNode[] {
    const files: TreeNode[] = [];
    for (const file of dir) {
      const path = directoryPath + "\\" + file;
      const data = this.loadProjectFile(path);
      const f = this.createFileTreeNode(data, file);
      files.push(f);
    }
    return files;
  }

  private createFileTreeNode(data: any, fileName: string) {
    const f = {
      "label": fileName,
      "icon": "fa-file-image-o",
      "data": data
    };
    return f;
  }

  private getFolderStructure(path: string) {
    const tree = listFolderStructure(path);
    return tree;
  }

  private generateMenu() {
    this.generateNewButtonMenu();
    this.generateTreeView(false);
  }

  private convertDoctypesToJSON(docTypes: string) {
    return JSON.parse(docTypes);
  }

  private checkIfFolderIsExpanded(currentTreeDirectory, index: number) {
    if (currentTreeDirectory.children[index] !== undefined) {
      return currentTreeDirectory.children[index]["expanded"] === true;
    } else {
      return false;
    }
  }
}

export interface DoctypeField {
  id: string;
  label: string;
  type: string;
}

export interface Doctype {
  name: string;
  label: string;
  type: string;
  fields: DoctypeField[];
  variants: string[];
}

export interface TreeNode {
  label?: string;
  data?: any;
  icon?: any;
  expandedIcon?: any;
  collapsedIcon?: any;
  children?: TreeNode[];
  leaf?: boolean;
  expanded?: boolean;
  type?: string;
  parent?: TreeNode;
  partialSelected?: boolean;
  styleClass?: string;
  draggable?: boolean;
  droppable?: boolean;
  selectable?: boolean;
}
