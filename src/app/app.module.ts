import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {MenubarModule} from "primeng/menubar";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormService} from "./services/form.service";
import {FilemanagerComponent} from "./filemanager/filemanager.component";
import {FileService} from "./services/file.service";
import {AppComponent} from "./app.component";
import {MainmenuComponent} from "./mainmenu/mainmenu.component";
import {FormComponent} from "./form/form.component";
import {FormfieldComponent} from "./formfield/formfield.component";
import {
  DragDropModule, FileUploadModule, InputSwitchModule, InputTextModule, PanelMenuModule,
  SlideMenuModule, TieredMenuModule
} from "primeng/primeng";
import {EditorModule} from "primeng/editor";
import {ButtonModule} from "primeng/button";
import {TreeModule} from "primeng/tree";
import {FormsModule} from "@angular/forms";
import {PathService} from "./services/path.service";
import {DialogModule} from "primeng/dialog";
import {GrowlModule} from "primeng/growl";
import {MessageService} from "primeng/components/common/messageservice";
import {RefreshService} from "./services/refresh.service";

@NgModule({
  declarations: [
    AppComponent,
    MainmenuComponent,
    FormComponent,
    FormfieldComponent,
    FilemanagerComponent,
  ],
  imports: [
    BrowserModule,
    MenubarModule,
    PanelMenuModule,
    BrowserAnimationsModule,
    SlideMenuModule,
    FileUploadModule,
    InputTextModule,
    EditorModule,
    InputSwitchModule,
    ButtonModule,
    DragDropModule,
    TieredMenuModule,
    TreeModule,
    FormsModule,
    DialogModule,
    GrowlModule
  ],
  providers: [FormService, FileService, PathService, MessageService, RefreshService],
  bootstrap: [AppComponent]
})
export class AppModule { }
