import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import {FormService} from "../services/form.service";
import {Doctype} from "../mainmenu/mainmenu.component";
import {FileService} from "../services/file.service";
import {MessageService} from "primeng/components/common/messageservice";
import * as $ from "jquery";
import {PathService} from "../services/path.service";
import {RefreshService} from "../services/refresh.service";

declare const saveFile: Function;

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.css"]
})
export class FormComponent implements OnInit {

  public docTypeData: Doctype = null;
  public activeFormData = null;
  display = false;
  savePath: string;
  items = "";
  stringText: string[] = [];


  constructor(public formService: FormService, public messageService: MessageService,
              public pathService: PathService, public refreshService: RefreshService) {
    this.formService.onChange = () => {
      this.docTypeData = this.formService.docTypeData;
      this.activeFormData = this.formService._activeFormData;
    };
  }

  ngOnInit() {
  }

  public onSubmit(form: NgForm) {
    const fieldValuesJSON = form.value;
    const savePath = fieldValuesJSON.savePath;
    delete fieldValuesJSON.savePath;
    const fieldValuesString: string = JSON.stringify(fieldValuesJSON, null, 2);
    const contentString = JSON.stringify(this.buildContentJSON(this.docTypeData.type,
      this.docTypeData.name, fieldValuesString), null, 2);
    const path = this.pathService.getProjectPath() + "\\assets\\html\\content";
    const typeDirectory = this.docTypeData.type + "s";
    const fullPath = path + "\\" + typeDirectory + "\\" + savePath;
    this.closeSaveDialog();
    try {
      if (!savePath) { throw Error; }
      saveFile(fullPath + ".json", contentString);
      const contentJSON = JSON.parse(contentString);
      this.refreshService.triggerRefresh(fullPath, contentJSON);
    } catch (e) {
      this.messageService.clear();
      this.messageService.add({severity: "error", summary: "Fehler",
        detail: "Fehler beim Speichern des Contents!"});
    }
  }

  private buildContentJSON(contentType: string, contentName: string, fieldValues: string): JSON {
    let templateFolder = contentType + "s";
    if (contentType === "page") {
      templateFolder = "layout";
    }
    const contentJSON = JSON.parse(`{
      "template" : "${templateFolder}/${contentName}",
      "data" : ${fieldValues}
    }`);
    const items = contentJSON["data"]["items"];
    if (items) {
      let itemsArr: string[];
      itemsArr = items.split(", ");
      contentJSON["data"]["items"] = itemsArr;
    }
    return contentJSON;
  }

  public onDropFile(event: Event) {
    console.log(event);
    const fieldContent = "Platzhalter";
    if (!this.items.includes(fieldContent)) {
      this.items += fieldContent;
    }
  }

  public showSaveDialog() {
    this.display = true;
  }

  private closeSaveDialog() {
    this.display = false;
    this.messageService.add({severity: "success", summary: "Content gespeichert",
      detail: "Der Content wurde erfolgreich gespeichert!"});
  }
}
